from django.db import models
from student.models import Student

# Create your models here.

class Course(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.student.first_name} {self.student.last_name} - Course: {self.name}"
