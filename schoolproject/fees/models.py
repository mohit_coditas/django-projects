from django.db import models
from student.models import Student

# Create your models here.

class Fees(models.Model):
    student = models.OneToOneField(Student, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.student.first_name} {self.student.last_name} - Fee: {self.amount}"
