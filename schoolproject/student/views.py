from django.shortcuts import render, get_object_or_404, redirect
from .models import Student
from .forms import StudentForm, FeeForm, CourseForm
from fees.models import Fees
from course.models import Course
# Create your views here.

def student_list(request):
    students = Student.objects.all()
    return render(request, 'student_list.html', {'students': students})

# def student_detail(request, pk):
#     student = get_object_or_404(Student, pk=pk)
#     return render(request, 'student_detail.html', {'student': student})

def student_detail(request, pk):
    student = get_object_or_404(Student, pk=pk)
    fee = Fees.objects.get(student=student)
    courses = Course.objects.filter(student=student)
    return render(request, 'student_detail.html', {'student': student, 'fee': fee, 'courses': courses})

def student_edit(request, pk):
    student = get_object_or_404(Student, pk=pk)
    fee = Fees.objects.get(student=student) if Fees.objects.filter(student=student).exists() else None
    courses = Course.objects.filter(student=student)
    
    if request.method == "POST":
        student_form = StudentForm(request.POST, request.FILES, instance=student)
        fee_form = FeeForm(request.POST, instance=fee)
        course_form = CourseForm(request.POST)
        
        if student_form.is_valid() and fee_form.is_valid() and course_form.is_valid():
            student = student_form.save(commit=False)
            student.save()

            if not fee:
                fee = fee_form.save(commit=False)
                fee.student = student
                fee.save()
            else:
                fee_form.save()

            course = course_form.save(commit=False)
            course.student = student
            course.save()

            return redirect('student_detail', pk=student.pk)
    else:
        student_form = StudentForm(instance=student)
        fee_form = FeeForm(instance=fee) if fee else FeeForm()
        course_form = CourseForm()

    return render(request, 'student_edit.html', {
        'student_form': student_form,
        'fee_form': fee_form,
        'course_form': course_form,
        'courses': courses
    })


def student_new(request):
    if request.method == "POST":
        student_form = StudentForm(request.POST, request.FILES)
        fee_form = FeeForm(request.POST)
        course_form = CourseForm(request.POST)

        if student_form.is_valid() and fee_form.is_valid() and course_form.is_valid():
            student = student_form.save(commit=False)
            student.save()

            fee = fee_form.save(commit=False)
            fee.student = student
            fee.save()

            course = course_form.save(commit=False)
            course.student = student
            course.save()

            return redirect('student_detail', pk=student.pk)
    else:
        student_form = StudentForm()
        fee_form = FeeForm()
        course_form = CourseForm()

    return render(request, 'student_edit.html', {
        'student_form': student_form,
        'fee_form': fee_form,
        'course_form': course_form,
    })

# def student_edit(request, pk):
#     student = get_object_or_404(Student, pk=pk)
#     if request.method == "POST":
#         form = StudentForm(request.POST, request.FILES, instance=student)
#         if form.is_valid():
#             student = form.save(commit=False)
#             student.save()
#             return redirect('student_detail', pk=student.pk)
#     else:
#         form = StudentForm(instance=student)
#     return render(request, 'student_edit.html', {'form': form})

def student_delete(request, pk):
    student = get_object_or_404(Student, pk=pk)
    student.delete()
    return redirect('student_list')
