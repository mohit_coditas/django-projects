from django import forms
from .models import Student
from fees.models import Fees
from course.models import Course

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'dob', 'student_photo', 'student_resume']
        widgets = {
            'dob': forms.TextInput(attrs={'type': 'date'}),
        }

class FeeForm(forms.ModelForm):
    class Meta:
        model = Fees
        fields = ['amount']

class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name']