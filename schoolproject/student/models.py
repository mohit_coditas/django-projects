from django.db import models

# Create your models here.

class Student(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    dob = models.DateField()
    student_photo = models.ImageField(upload_to='student_photos/', blank=True, null=True)
    student_resume = models.FileField(upload_to='student_resumes/', blank=True, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
